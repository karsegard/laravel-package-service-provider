<?php

namespace KDA\Laravel\Models\Traits;


trait HasConfigurableTableName
{

    public function initializeHasConfigurableTableName()
    {
        $this->table = $this->getConfigurableTableName();
    }
}


