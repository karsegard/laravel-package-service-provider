<?php

namespace KDA\Laravel\Traits;

use KDA\Dump\Facades\Dump;

trait HasDumps
{

    public function initializeHasDumps(): void
    {

        if (!property_exists($this, 'dumps')) {
            $this->dumps = [];
        }
    }



    public function bootHasDumps(): void
    {
        if (class_exists(Dump::class)) {
            foreach ($this->getDumps() as $dump) {
                Dump::register($dump,$this);
            }
        }
    }

    public function getDumps():array{
        return $this->dumps;
    }

    public function getRegisteredDumps(){
        return $this->dumps??[];
    }
}
