<?php

namespace KDA\Laravel\Traits;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;


trait RequiresResourcesDir
{


    public function checkResourceDir(): void
    {
        if (!property_exists($this, 'resourcesDir')) {
            $this->resourcesDir = 'resources';
        }
        
    }

    


}
