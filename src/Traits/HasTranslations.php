<?php

namespace KDA\Laravel\Traits;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;


trait HasTranslations
{

    use RequiresPackageName;
    use RequiresResourcesDir;

    public function initializeHasTranslations(): void
    {
        $this->checkPackageName();
        $this->checkResourceDir();
      
        if (!property_exists($this, 'translationsDir')) {
            $this->translationsDir =  $this->resourcesDir . '/lang';
        }

        if (!property_exists($this, 'publishTranslationsTo')) {
            $this->publishTranslationsTo = 'vendor/'.$this->packageName;
        }
        
    }

    

    public function bootHasTranslations(): void
    {
        $this->loadTranslationsFrom($this->path($this->translationsDir), $this->packageName);
        $this->registerPublishableTranslations();
        
    }

    public function registerPublishableTranslations():void
    {

        $translations = $this->getFiles($this->translationsDir);
        foreach ($translations as $view) {
            $sourcedir = $this->path($this->translationsDir);
            $dest =  str_replace($sourcedir, $this->publishTranslationsTo, $view);

            $this->publishable['lang'][$view] = lang_path($dest);
        }
    }

}
