<?php

namespace KDA\Laravel\Traits;


trait HasConfig
{

    public function initializeHasConfig(): void
    {
        if (!property_exists($this, 'configDir')) {
            $this->configDir = 'config';
        }
        if (!property_exists($this, 'configs')) {
            $this->configs = [];
        }
    }

    public function bootHasConfig(): void
    {
        $this->registerConfigs();
        foreach ($this->configs as $config => $key) {
            $fullpath =  $this->path($this->configDir, $config);
            $dest = config_path($config);
            $this->publishable['config'][$fullpath] = $dest;
        }
    }


    public function registerConfigs()
    {
        if (property_exists($this, 'configs')) {
            foreach ($this->configs as $file => $key) {
                if (strrpos($file, '.php') !== false) {
                    $this->mergeConfigFrom(
                        $this->path($this->configDir, $file),
                        $key
                    );
                }
            }
        }
    }
}
