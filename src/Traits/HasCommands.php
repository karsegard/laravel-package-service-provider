<?php

namespace KDA\Laravel\Traits;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;


trait HasCommands
{

    public function initializeHasCommands(): void
    {
        if (!property_exists($this, '_commands')) {
            $this->_commands = [];
        }
    }

    

    public function bootHasCommands(): void
    {
        if ($this->app->runningInConsole()) {
            $this->registerConsoleCommands();
        }


    }

    public function registerConsoleCommands (){
        foreach($this->_commands as $command){
            $this->commands($command);
        }
    }

}
