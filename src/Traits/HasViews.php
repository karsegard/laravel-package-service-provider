<?php

namespace KDA\Laravel\Traits;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;


trait HasViews
{

    use RequiresResourcesDir;

    public function initializeHasViews(): void
    {
        $this->checkResourceDir();
        if (!property_exists($this, 'viewsDir')) {
            $this->viewsDir = $this->resourcesDir . '/views';
        }

        if (!property_exists($this, 'publishViewsTo')) {
            if(!empty($this->packageName)){
               $this->publishViewsTo =  'vendor/'.$this->packageName;
            }else{
               $this->publishViewsTo =  '';
            }
        }
    }



    public function bootHasViews(): void
    {

        $this->loadPublishableViews();
        $this->doRegisterViews();
       

    }

    
    public function loadPublishableViews()
    {
        $views = $this->getFiles($this->viewsDir);

        foreach ($views as $view) {
            $sourcedir = $this->path($this->viewsDir);
            $dest = 'views' . $this->leadSlashes(str_replace($sourcedir, $this->publishViewsTo, $view));
            $this->publishable['views'][$view] = resource_path($dest);
        }
    }


    public function doRegisterViews()
    {

        if (property_exists($this, 'viewNamespace') && is_string($this->viewNamespace)) {
            $this->loadViewsFrom($this->path($this->viewsDir), $this->viewNamespace);
        }else if (property_exists($this, 'registerViews') && is_string($this->registerViews)) {
            $this->loadViewsFrom($this->path($this->viewsDir), $this->registerViews);
        }else if (property_exists($this, 'packageName') && is_string($this->packageName)) {
            $this->loadViewsFrom($this->path($this->viewsDir), $this->packageName);
        }
    }
}
