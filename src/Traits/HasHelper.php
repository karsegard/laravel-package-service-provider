<?php

namespace KDA\Laravel\Traits;


trait HasHelper
{

    public function initializeHasHelper(): void
    {
        if (!property_exists($this, 'helperDir')) {
            $this->helperDir = 'helpers';
        }
     
    }

    public function bootHasHelper(): void
    {
        $this->loadHelpers();

    }

    protected function localHelperPath($helper=''){
        return  $this->path($this->helperDir,$helper);
    }

    protected function loadHelper($helper){
        require_once $this->localHelperPath($helper);
    }

    protected function loadHelpers()
    {   
        $helpers =glob($this->path($this->helperDir).'/*.php');
        foreach ($helpers as $filename) {
            
            require_once $filename;
        }
    }



}
