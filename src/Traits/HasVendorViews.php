<?php

namespace KDA\Laravel\Traits;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;


trait HasVendorViews
{

    use RequiresResourcesDir;

    public function initializeHasVendorViews(): void
    {
        $this->checkResourceDir();
        if (!property_exists($this, 'vendorViewsDir')) {
            $this->vendorViewsDir = $this->resourcesDir . '/vendor_views';
        }

        if (!property_exists($this, 'publishVendorViewsTo')) {
            $this->publishVendorViewsTo = '';
        }
    }



    public function bootHasVendorViews(): void
    {
        $this->loadPublishableVendorViews();
        $this->doRegisterVendorViews();
    }

    
    public function loadPublishableVendorViews()
    {
        $views = $this->getFiles($this->vendorViewsDir);
        foreach ($views as $view) {
            $sourcedir = $this->path($this->vendorViewsDir);
            $dest = 'views' . $this->leadSlashes(str_replace($sourcedir, $this->publishVendorViewsTo, $view));
            $this->publishable['vendor_views'][$view] = resource_path($dest);
        }
    }


    public function doRegisterVendorViews()
    {

        if (property_exists($this, 'vendorViewsNamespace') && is_string($this->vendorViewsNamespace)) {
            $this->loadViewsFrom($this->path($this->vendorViewsDir), $this->vendorViewsNamespace);
        }
    }
}
