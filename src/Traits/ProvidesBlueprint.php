<?php

namespace KDA\Laravel\Traits;

use Illuminate\Database\Schema\Blueprint;

trait ProvidesBlueprint
{

    public function initializeProvidesBlueprint(): void
    {
        if (!property_exists($this, 'blueprints')) {
            $this->blueprints = [];
        }
    }



    public function bootProvidesBlueprint(): void
    {
        foreach ($this->blueprints as $macro => $closure) {
            Blueprint::macro($macro, $closure);
        }
    }
}
