<?php

namespace KDA\Laravel\Traits;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;


trait HasObservers
{

    public function initializeHasObservers(): void
    {
        if (!property_exists($this, 'observers')) {
            $this->observers = [];
        }
    }



    public function bootHasObservers(): void
    {
        foreach ($this->observers as $item) {
            foreach ($item as $model => $observer) {
                call_user_func($model . '::observe', $observer);
            }
        }
    }
}
