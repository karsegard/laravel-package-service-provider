<?php

namespace KDA\Laravel\Traits;

use Backpack\CRUD\app\Console\Commands\Version;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Blade;

trait HasComponentNamespaces
{

    public function initializeHasComponentNamespaces(): void
    {
        if (!property_exists($this, 'componentNamespaces')) {
            $this->componentNamespaces = [];
        }
        if (!property_exists($this, 'anonymousComponentNamespaces')) {
            $this->anonymousComponentNamespaces = [];
        }
    }



    public function bootHasComponentNamespaces(): void
    {
        foreach ($this->componentNamespaces as $namespace => $blade) {
            Blade::componentNamespace($namespace, $blade);
        }
        if (\KDA\Laravel\is_laravel_version_9_or_upper()) {
            foreach ($this->anonymousComponentNamespaces as $namespace => $blade) {
                Blade::anonymousComponentNamespace($namespace, $blade);
            }
        }
    }
}
