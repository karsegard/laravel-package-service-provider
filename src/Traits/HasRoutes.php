<?php

namespace KDA\Laravel\Traits;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use Illuminate\Routing\Router;

trait HasRoutes
{

    public function initializeHasRoutes(): void
    {

        if (!property_exists($this, 'routesDir')) {
            $this->routesDir = 'routes';
        }

        if (!property_exists($this, 'routes')) {
            $this->routes = [];
        }
    }



    public function bootHasRoutes(): void
    {
        $this->setupRoutes($this->app->router);
    }

    public function publishRoutes($group='routes'){
        $this->registerPublishable($group,collect($this->routes)->mapWithKeys(function($item){
            return [$this->path($this->routesDir,$item)=>base_path('routes').$item];
        })->toArray());
    }

   
    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function setupRoutes(Router $router)
    {
        
        foreach ($this->routes as $routeFile) {
            $file = $this->path($this->routesDir,$routeFile);
           
            if (file_exists(base_path('routes') . $routeFile)) {

                $file = base_path('routes') . $routeFile;
            }

            $this->loadRoutesFrom($file);
        }
    }
}
