<?php

namespace KDA\Laravel\Traits;


trait HasYamlConfig
{

    public function initializeHasYamlConfig(): void
    {
        if (!property_exists($this, 'configDir')) {
            $this->configDir = 'config';
        }
        if (!property_exists($this, 'configs')) {
            $this->configs = [];
        }
    }

    public function bootHasYamlConfig(): void
    {
        $this->registerYamlConfigs();
        foreach ($this->configs as $config => $key) {
            $fullpath =  $this->path($this->configDir, $config);
            $dest = config_path($config);
            $this->publishable['config'][$fullpath] = $dest;
        }
    }


    public function registerYamlConfigs()
    {

        $yaml = $this->app->make('pragmarx.yaml');
        if (property_exists($this, 'configs')) {
            foreach ($this->configs as $file => $key) {

                if (strrpos($file, '.yaml') !== false || strrpos($file, '.yml') !== false) {
                    $yaml->loadToConfig($this->path($this->configDir, $file), $key);
                }
            }
        }
    }
}
