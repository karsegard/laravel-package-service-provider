<?php

namespace KDA\Laravel\Traits;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;


trait HasManifest
{
    use RequiresPackageName;
    public function initializeHasManifest(): void
    {
        if (!property_exists($this, 'publishAssetsTo')) {
            $this->checkPackageName();
            $this->publishAssetsTo = 'vendor/'.$this->packageName;
        }
    }

    public function bootHasManifest(): void
    {
        //$assets = $this->getFiles($this->assetsDir );
        $manifest = $this->path('mix-manifest.json');
        $sourcedir = $this->path('');
        $dest= str_replace($sourcedir,'',$manifest);
        //dd($manifest,$sourcedir,$dest);
        $this->publishable['assets'][$manifest] = public_path($this->publishAssetsTo.'/'.$dest);
        
        /*foreach ($assets as $asset) {
            $sourcedir = $this->path($this->assetsDir);
            $dest= 'assets'.str_replace($sourcedir,'',$asset);
            $this->publishable['assets'][$asset] = public_path($this->publishAssetsTo.'/'.$dest);
        }*/
        
    }

}
