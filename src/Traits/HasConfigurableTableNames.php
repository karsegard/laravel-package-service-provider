<?php

namespace KDA\Laravel\Traits;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;


trait HasConfigurableTableNames
{
   
    public function initializeHasConfigurableTableNames(): void
    {

    }

    

    public function bootHasConfigurableTableNames(): void
    {
       
    }

    public static function getTableName($name){
        $config = collect(self::$tables_config_key)->first();
        $table = config($config)[$name] ?? null;
        if(!$table){
            throw new \Exception('You are using configurable table names and didnt set the config for table '.$name);
        }
        return $table;
    }

}
