<?php

namespace KDA\Laravel\Traits;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;


trait HasProviders
{

    public function initializeHasProviders(): void
    {
        if (!property_exists($this, 'additionnalProviders')) {
            throw new \Error('package require additionnalProviders property to be  set');
        }
    }

    
    public function registerHasProviders(): void
    {
        foreach($this->additionnalProviders as $provider) {
            $this->app->register($provider);
        }
    }

}
