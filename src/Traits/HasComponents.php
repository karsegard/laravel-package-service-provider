<?php

namespace KDA\Laravel\Traits;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Blade;

trait HasComponents
{

    public function initializeHasComponents(): void
    {
        if (!property_exists($this, 'components')) {
            $this->components = [];
        }
    }

    

    public function bootHasComponents(): void
    {
        foreach($this->components as $key=>$value){
           Blade::component($key,$value);
        }
    }

  

}
