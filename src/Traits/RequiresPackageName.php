<?php

namespace KDA\Laravel\Traits;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;


trait RequiresPackageName
{


    public function checkPackageName(): void
    {
        if (!property_exists($this, 'packageName')) {
            throw new \Error('package require packageName property to be  set');
        }
       
        
    }

    public function getPackageName():string{
        return $this->packageName;
    }
    


}
