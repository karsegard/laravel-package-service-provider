<?php

namespace KDA\Laravel\Traits;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;


trait HasAssets
{
    use RequiresPackageName;
    public function initializeHasAssets(): void
    {
       
        if (!property_exists($this, 'assetsDir')) {
            $this->assetsDir = 'assets';
        }

        if (!property_exists($this, 'publishAssetsTo')) {
            $this->checkPackageName();
            $this->publishAssetsTo = 'vendor/'.$this->packageName;
        }
    }

    public function getAssetVendorPath($path){
        return asset(str($this->publishAssetsTo)->finish('/').$path);
    }

    public function bootHasAssets(): void
    {
        $assets = $this->getFiles($this->assetsDir );
        
        foreach ($assets as $asset) {
            $sourcedir = $this->path($this->assetsDir);
            $dest= 'assets'.str_replace($sourcedir,'',$asset);
            $this->publishable['assets'][$asset] = public_path($this->publishAssetsTo.'/'.$dest);
        }
    }

}
