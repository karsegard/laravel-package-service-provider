<?php

namespace KDA\Laravel\Traits;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;


trait HasLoadableMigration
{
    protected static bool $migration_enabled = true;

    public static function enableMigrations($bool=true):void
    {
        static::$migration_enabled = $bool;
    }
    public function initializeHasLoadableMigration(): void
    {
        if (!property_exists($this, 'migrationDir')) {
            $this->migrationDir = 'database/migrations';
        }
    }


    public function bootHasLoadableMigration(): void
    {
        //  if ($this->app->runningInConsole()) {
        $hasMigrations = in_array(HasMigration::class, class_uses_recursive(static::class));
        if ((method_exists($this, 'shouldLoadMigration') && !$this->shouldLoadMigration()) || !static::$migration_enabled) {
            return;
        }
        if (!$hasMigrations || $this->internalShouldLoadMigration()) {
            $this->loadMigrationsFrom($this->path($this->migrationDir));
        }
        //  }
    }
    /*
        Determine if migrations have been published
    */
    protected function internalShouldLoadMigration(): bool
    {
        $filesystem = $this->app->make(Filesystem::class);
        $migrations = $this->getFiles($this->migrationDir);
        foreach ($migrations as $migration) {
            $file =  $this->getTimelessMigrationFileName(basename($migration));
            $col = Collection::make($this->app->databasePath() . DIRECTORY_SEPARATOR . 'migrations' . DIRECTORY_SEPARATOR)
                ->flatMap(function ($path) use ($filesystem, $file) {
                    return $filesystem->glob($path . '*_' . $file);
                })->first();
            if (!blank($col)) {
                return false;
            }
        }
        return true;
    }
}
