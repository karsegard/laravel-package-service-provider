<?php

namespace KDA\Laravel\Traits;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;

trait HasMigration
{

    protected $timestampRegex= '/([\d]{4}_[\d]{2}_[\d]{2}_[\d]{6}_)/m';

    public function initializeHasMigration(): void
    {

        if (!property_exists($this, 'migrationDir')) {
            $this->migrationDir = 'database/migrations';
        }
    }


    public function bootHasMigration(): void
    {

        if ($this->app->runningInConsole()) {
           $this->registerPublishableMigrations();
        }
    }

    public function registerPublishableMigrations():void
    {
        $migrations = $this->getFiles($this->migrationDir);
        foreach ($migrations as $migration) {
            $this->publishable['migrations'][$migration] = $this->getMigrationFileName(basename($migration));
        }
    }

    protected function getTimelessMigrationFileName($migrationFileName): string
    {
        return preg_replace($this->timestampRegex, '', $migrationFileName);
    }

    protected function migrationFileHasTimestamp($migrationFileName): bool
    {
        return preg_match($this->timestampRegex, $migrationFileName) !== false;
    }

    protected function getMigrationFileName($migrationFileName): string
    {


        $timestamp = date('Y_m_d_His');

        $filesystem = $this->app->make(Filesystem::class);
        $newFileName = $this->app->databasePath() . "/migrations/{$timestamp}_{$migrationFileName}";
        if ($this->migrationFileHasTimestamp($migrationFileName)) {
            // filename already have a timestamp
            $newFileName =  $this->app->databasePath() . "/migrations/" . preg_replace($this->timestampRegex, $timestamp . "_", $migrationFileName);
        }

        $timelessMigrationFileName = $this->getTimelessMigrationFileName($migrationFileName);
        return Collection::make($this->app->databasePath() . DIRECTORY_SEPARATOR . 'migrations' . DIRECTORY_SEPARATOR)
            ->flatMap(function ($path) use ($filesystem, $timelessMigrationFileName) {
                return $filesystem->glob($path . '*_' . $timelessMigrationFileName);
            })
            ->push($newFileName)
            ->first();
    }
}
