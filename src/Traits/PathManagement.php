<?php

namespace KDA\Laravel\Traits;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;


trait PathManagement
{

    public function initializePathManagement(): void
    {
      
    }



    public function bootPathManagement(): void
    {
       
    }

    public function leadSlashes ($path): string{
        if($path[0]!==DIRECTORY_SEPARATOR){
            return DIRECTORY_SEPARATOR.$path;
        }
        return $path;
    }

    public function os_path_join(...$parts)
    {
        return preg_replace('#' . DIRECTORY_SEPARATOR . '+#', DIRECTORY_SEPARATOR, implode(DIRECTORY_SEPARATOR, array_filter($parts)));
    }

    protected function path(...$comp)
    {
        return $this->os_path_join($this->packageBaseDir(), ...$comp);
    }

    protected function getFiles($dir)
    {
        $path = $this->os_path_join($this->packageBaseDir(), $dir);
     
        $result = [];

        if (file_exists($path)) {
            $it = new \RecursiveDirectoryIterator($path);
            foreach (new \RecursiveIteratorIterator($it) as $file) {
                if ($file->isFile()) {
                    $result[] = $file->getPathname();
                }
            }
        }

        return $result;
    }
}
