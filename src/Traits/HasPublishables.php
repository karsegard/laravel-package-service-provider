<?php

namespace KDA\Laravel\Traits;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;


trait HasPublishables
{

    public function initializeHasPublishables(): void
    {
        if (!property_exists($this, 'publishable')) {
            $this->publishable = [];
        }
    }


    protected function registerPublishable(string $group, array $paths = [])
    {
        $this->initializeHasPublishables();
        $this->publishable[$group] = isset($this->publishable[$group])  ? array_merge($this->publishable[$group], $paths) : $paths;
    }

    protected function registerPublishableResources()
    {
        $this->initializeHasPublishables();

        if ($this->app->runningInConsole()) {
            foreach ($this->publishable as $group => $paths) {
                $this->publishes($paths, $group);
            }
        }
    }

    public function clearPublishableGroup($group){
        $this->publishable[$group]=[];
    }

    public function registerPublishableFolder($group,$folder,$destination_callback=NULL):void
    {
        if(!$destination_callback){
            $destination_callback = function($dest){
                return base_path($dest);
            };
        }

        $files = $this->getFiles($folder);
        foreach ($files as $file) {
            $sourcedir = $this->path($folder);
            $dest = $folder . $this->leadSlashes(str_replace($sourcedir, '', $file));
            $this->publishable[$group][$file] = $destination_callback($dest);
        }
    }
}
