<?php

namespace KDA\Laravel\Traits;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;


trait HasPublicAssets
{

    public function initializeHasPublicAssets(): void
    {
      
        if (!property_exists($this, 'publicDir')) {
            $this->publicDir = 'public';
        }
    }

    

    public function bootHasPublicAssets(): void
    {
        $assets = $this->getFiles($this->publicDir );
        
        foreach ($assets as $asset) {
            $sourcedir = $this->path($this->publicDir);
            $dest= str_replace($sourcedir,'',$asset);

            $this->publishable['public'][$asset] = public_path($dest);
        }
      //  dd( $this->publishable['public']);
      
    }

}
