<?php

namespace KDA\Laravel\Traits;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;


trait HasSpatieSettings
{
    use RequiresPackageName;
    use HasPublishables;

    protected $spatieSettingsMigrationsDir="database/settings";
    public function initializeHasSpatieSettings(): void
    {
        $this->checkPackageName();
        if(!\Composer\InstalledVersions::isInstalled('spatie/laravel-settings')){
            throw new \Exception("Package ".$this->packageName." is using Trait HasSpatieSettings. Please add the package to dependencies \"composer require spatie/laravel-settings\"" );
        }
        $this->registerSpatieSettingsMigration();

        if (!property_exists($this, 'spatieSettingsClass')) {
            $this->spatieSettingsClass = [];
        }

        $this->registerSpatieSettings();
    }

    public function registerSpatieSettingsMigration(){
        app()->afterResolving('files', function () {
            $this->clearPublishableGroup('settings-migrations');
            // $this->registerPublishableFolder('settings-migrations',$this->spatieSettingsMigrationsDir);
            $migrations = $this->getFiles($this->spatieSettingsMigrationsDir);
            foreach ($migrations as $migration) {
                $this->publishable['settings-migrations'][$migration] = $this->getSpatieMigrationSettingFileName(basename($migration));
            }

        });
    }
    
    public function registerSpatieSettings() : void{
        $settings  = config()->get('settings.settings') ?? [];

        $settings = collect($settings)->merge($this->spatieSettingsClass)->toArray();

        config()->set('settings.settings',$settings);
    }
      

    protected function getSpatieMigrationSettingFileName($migrationFileName)
    {
        $timestamp = date('Y_m_d_His');
        $filesystem =app()->make(Filesystem::class);
      

        return Collection::make(app()->databasePath() . DIRECTORY_SEPARATOR . 'settings' . DIRECTORY_SEPARATOR)
            ->flatMap(function ($path) use ($filesystem, $migrationFileName) {
                return $filesystem->glob($path . '*_' . $migrationFileName);
            })
            ->push(app()->databasePath() . "/settings/{$timestamp}_{$migrationFileName}")
            ->first();
    }
}
