<?php

namespace KDA\Laravel\Traits;



trait HasDynamicSidebar
{

    public function initializeHasDynamicSidebar(): void
    {

        if (!property_exists($this, 'sidebars')) {
            $this->sidebars = [];
        }
    }



    public function bootHasDynamicSidebar(): void
    {
        if (class_exists("\KDA\Backpack\DynamicSidebar\Facades\Sidebar")) {
            \KDA\Backpack\DynamicSidebar\Facades\Sidebar::registerSidebars($this,$this->sidebars);
        }
    }

    public function getRegisteredSidebars(){
        return $this->sidebars??[];
    }
}
