<?php
namespace KDA\Laravel\Contracts;

interface CanIgnoreMigration {
    public function shouldLoadMigration();
}