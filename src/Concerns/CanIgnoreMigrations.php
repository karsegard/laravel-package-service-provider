<?php

namespace KDA\Laravel\Concerns;


trait CanIgnoreMigrations
{
    public bool $runMigrations = true;

    public function ignoreMigrations(): self
    {
        $this->runMigrations = false;

        return $this;
    }

    public function shouldRunMigrations()
    {
        return $this->runMigrations;
    }
}
