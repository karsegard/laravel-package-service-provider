<?php

namespace KDA\Laravel;


use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;


if (!function_exists('is_laravel_version_8')) {
    
    function is_laravel_version_8()
    {
        return version_compare(app()->version(),'8','=') ;
    }
}

if (!function_exists('is_laravel_version_9')) {
    
    function is_laravel_version_9()
    {
        return version_compare(app()->version(),'9','=') ;
    }
}

if (!function_exists('is_laravel_version_9_or_upper')) {
    
    function is_laravel_version_9_or_upper()
    {
        return version_compare(app()->version(),'9','>=') ;
    }
}
class PackageServiceProvider extends ServiceProvider
{
    use Traits\HasPublishables;

    use Traits\PathManagement;
    protected $packageBaseDir;


    protected $boots = [];
    protected $registers = [];
    protected $publishable = [];
    
    /*
    protected function packageBaseDir()
    {
        $reflector = new \ReflectionClass(get_class($this));

        return dirname($reflector->getFileName(),2);
    }
*/

   
    public function __construct($app)
    {

        $reflection = new  \ReflectionClass($this);
        
        foreach ($reflection->getTraits() as $trait) {
            
            $name = str_replace($trait->getNamespaceName() . "\\", "", $trait->getName());
            $this->boots[] = "boot" . $name;
            $this->registers[] = "register" . $name;

            $init = "initialize" . $name;
            if (method_exists($this, $init)) {
                $this->$init();
            }
        }
        parent::__construct($app);

        $this->setUp();
    }

    public function setUp():void
    {

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        foreach ($this->registers as $register) {
            if (method_exists($this, $register)) {
                $this->$register();
            }
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $postRegister = 'postRegister';
        if (method_exists($this, $postRegister)) {
            $this->$postRegister();
        }
        foreach ($this->boots as $boot) {
            if (method_exists($this, $boot)) {

                $this->$boot();
            }
        }
        $bootSelf='bootSelf';
        if (method_exists($this, $bootSelf)) {
            $this->$bootSelf();
        }
    
        $this->registerPublishableResources();
    }


   
 
}
