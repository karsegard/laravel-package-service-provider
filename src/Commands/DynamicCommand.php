<?php

namespace KDA\Laravel\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputOption;

class DynamicCommand extends Command
{
    use Traits\IsDynamic;

    
    public function __construct()
    {
        $this->signature = $this->getSignature();
        parent::__construct();
    }

   
}
